"""Custom client handling, including SuisseBoundariesStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk.streams import Stream


class SuisseBoundariesStream(Stream):
    """Stream class for SuisseBoundaries streams."""

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of record-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """
        # Step 1: use gdal/ogr to open file from remote

        from osgeo import ogr, osr

        driver = ogr.GetDriverByName('OpenFileGDB')
        file = r"/vsizip//vsicurl/https://data.geo.admin.ch/ch.swisstopo.swissboundaries3d/swissboundaries3d_2023-01/swissboundaries3d_2023-01_2056_5728.gdb.zip/swissBOUNDARIES3D_1_4_LV95_LN02.gdb"
        dataSource = driver.Open(file, 0)
        muns = dataSource.GetLayer('TLM_HOHEITSGEBIET')

        source = osr.SpatialReference()
        source.ImportFromEPSG(2056)

        target = osr.SpatialReference()
        target.ImportFromEPSG(4326)

        # Step 2: yield features, splice in geometry as geom_wkt as WKT in WGS84
        for feature in muns:
            geom = feature.GetGeometryRef()
            geom.Transform(osr.CoordinateTransformation(source, target))

            feat = dict(feature)
            feat["GEOM_PLY_WKT"] = geom.ExportToWkt()
            feat["GEOM_CTR_WKT"] = geom.Centroid().ExportToWkt()
            yield feat
