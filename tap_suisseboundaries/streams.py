"""Stream type classes for tap-suisseboundaries."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_suisseboundaries.client import SuisseBoundariesStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class MunStream(SuisseBoundariesStream):
    name = "municipalities"
    primary_keys = ["BFS_NUMMER"]
    replication_key = "DATUM_AENDERUNG"
    schema_filepath = SCHEMAS_DIR / "muns.json"
